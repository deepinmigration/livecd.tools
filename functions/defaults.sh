export LB_UNION_FILESYSTEM="${LB_UNION_FILESYSTEM:-aufs}"
export LB_CHROOT_FILESYSTEM="${LB_CHROOT_FILESYSTEM:-squashfs}"
export DEBIAN_FRONTEND=noninteractive

case "${LB_ARCHITECTURE}" in
    amd64)
        export LB_LINUX_FLAVOURS="amd64"
        ;;
    i386)
        export LB_LINUX_FLAVOURS="i586-pae"
        ;;
    *)
        LB_LINUX_FLAVOURS="${LB_LINUX_FLAVOURS:-486}"
esac
export LB_LINUX_FLAVOURS

case "${LB_MODE}" in
    ubuntu)
        LB_LINUX_PACKAGES="${LB_LINUX_PACKAGES:-linux}"
        ;;
    *)
        LB_LINUX_PACKAGES="${LB_LINUX_PACKAGES:-linux-image}"
        ;;
esac
export LB_LINUX_PACKAGES

export LIVE_IMAGE_TYPE="${LIVE_IMAGE_TYPE:-iso-hybrid}"
export LB_BOOTLOADER="${LB_BOOTLOADER:-syslinux}"

case "${LB_INITRAMFS}" in
    live-boot)
        LB_BOOTAPPEND_LIVE="${LB_BOOTAPPEND_LIVE:-boot=live components quiet splash}"
        LB_BOOTAPPEND_LIVE_FAILSAFE="${LB_BOOTAPPEND_LIVE_FAILSAFE:-boot=live components memtest noapic noapm nodma nomce nolapic nomodeset nosmp nosplash vga=normal}"
        ;;
    casper)
        LB_BOOTAPPEND_LIVE="${LB_BOOTAPPEND_LIVE:-boot=casper quiet splash}" 
        LB_BOOTAPPEND_LIVE_FAILSAFE="${LB_BOOTAPPEND_LIVE_FAILSAFE:-boot=casper memtest noapic noapm nodma nomce nolapic nomodeset nosmp nosplash vga=normal}"
        ;;
    none)
        LB_BOOTAPPEND_LIVE="${LB_BOOTAPPEND_LIVE:-quiet splash}"
        LB_BOOTAPPEND_LIVE_FAILSAFE="${LB_BOOTAPPEND_LIVE_FAILSAFE:-memtest noapic noapm nodma nomce nolapic nomodeset nosmp nosplash vga=normal}"
        ;;
esac

if [ "${LB_UNION_FILESYSTEM}" != "aufs" ];then
    LB_BOOTAPPEND_LIVE="${LB_BOOTAPPEND_LIVE} union=${LB_UNION_FILESYSTEM}"
    LB_BOOTAPPEND_LIVE_FAILSAFE="${LB_BOOTAPPEND_LIVE_FAILSAFE} union=${LB_UNION_FILESYSTEM}"
fi

export LB_BOOTAPPEND_LIVE
export LB_BOOTAPPEND_LIVE_FAILSAFE
