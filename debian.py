#! /usr/bin/env python3
#

import os
import subprocess
import sys
import glob

# TODO:auto detect program abspath.
Path=os.path.dirname(os.path.realpath(__file__))
sys.path.append(Path)

import misc
from misc import environment, execute
from misc import DebootstrapException, InitException, ExecutionException
from misc import ColoredOutput

if sys.stderr.isatty():
    sys.stderr = ColoredOutput(sys.stderr, 'red')
    print_indicator = ColoredOutput(sys.stdout, 'yellow')
else:
    print_indicator = sys.stdout

class DebianBuilder():
    def __init__(self, architecture=None, name=None):
        if not os.path.isfile('config/common'):
            raise InitException("config/common not exists")

        self.path = Path
        self.environment = environment('config/common')
        self.architecture = architecture if architecture else self.environment.get('LB_ARCH')
        if self.architecture is None:
            _arch = subprocess.check_output("dpkg --print-architecture", shell=True).strip()
            self.architecture = bytes.decode(_arch)
        self.distribution = self.environment.get('LB_DISTRIBUTION')
        assert self.distribution is not None
        self.build_path = 'chroot'
        self.name = name

    def create_progress(self, progress):
        progresses = [
                "debootstrap",
                "setup_chroot",
                "chroot_local-package-list",
                "chroot_packages-list",
                "chroot_linux-image",
                "chroot_install-packages",
                "chroot_hooks",
                "chroot_includes",
                "binary_local-packages-list",
                "binary_packages-list",
                "binary_includes",
                "binary_linux-image",
                "binary_chroot",
                "binary_manifest",
                "binary_archives",
                "binary_rootfs",
                "binary_syslinux",
                "binary_grub",
                "binary_efi_grub2",
                "binary_disk",
                "binary_iso"
                ]
        progresses_message = {
                "debootstrap": "Bootstrap a basic system",
                "setup_chroot": "Setup chroot runtime and environment",
                "chroot_local-packages-list": "Generate local package list",
                "chroot_packages-list": "Generate package install list",
                "chroot_linux-image": "Generate suitable kernel image",
                "chroot_install-packages": "Installing packages",
                "chroot_hooks": "Invoke chroot hooks",
                "chroot_includes": "Copy chroot override files",
                "binary_packages-list": "Binary package lists",
                "binary_includes": "Save binary includes",
                "binary_linux-image": "Save binary linux image",
                "binary_chroot": "Generate chroot filesystme",
                "binary_manifest": "Creating mainfest file",
                "binary_archives": "Prepare binary archives",
                "binary_rootfs": "Building root filesystem image",
                "binary_syslinux": "Installing syslinux",
                "binary_grub": "Installing grub",
                "binary_efi_grub2": "Installing grub efi",
                "binary_disk": "Installing disk information",
                "binary_iso": "Building iso image"
                }
        if progress in progresses:
            indicator = progresses.index(progress) + 1
        else:
            indicator = -1
        message = progresses_message.get(progress, "Leak of progess message")
        print_indicator.write('%d/%d: %s\n' % (indicator, len(progresses), message))

        with open('.build/progress', 'w') as fp:
            fp.write('%d/%d: %s\n' % (indicator, len(progresses), message))

    def chroot_config(self):
        self.env = os.environ.copy()
        for item in self.environment:
            self.env[item] = self.environment[item]
        self.env['LB_ARCHITECTURE'] = self.architecture
        self.env['PROGRAM'] = self.path
        os.makedirs('.build', exist_ok=True)
        open('.build/config', 'w').close()

    def debootstrap(self,debootrap_mirror=None):
        self.create_progress('debootstrap')
        if not os.path.isfile('/usr/sbin/debootstrap'):
            raise DebootstrapException('/usr/sbin/debootstrap not exist.')

        debootrap_mirror = debootrap_mirror or self.env.get('LB_DEBOOTSTRAP_MIRROR')
        if debootrap_mirror is None:
            raise DebootstrapException("debootrap_mirror should not be none.")

        debootstrap_options_early = "--arch=" + self.architecture
        debootstrap_options_late = self.distribution + ' ' + self.build_path  + ' ' + debootrap_mirror
        debootstrap_options_early = debootstrap_options_early + ' --components=' + self.env.get('LB_DEBOOTSTRAP_AREA').replace(' ',',')

        debootstrap_args = self.environment.get('LB_DEBOOTSTRAP_OPTIONS')
        if debootstrap_args:
            debootstrap_options = debootstrap_options_early + ' ' + debootstrap_args + ' ' + debootstrap_options_late
        else:
            debootstrap_options = debootstrap_options_early + ' ' + debootstrap_options_late

        if os.path.isfile('.build/bootstrap'):
            if not os.path.exists(os.path.join(self.build_path, 'var/lib/dpkg')):
                sys.stdout.write('W: bootstrap seems not done - run again\n')
                execute('rm -f .build/*')
            else:
                sys.stdout.write('I: bootstrap already done - noting to do\n')
                return

        use_tmpfs = self.environment.get('USE_TMPFS')
        if not os.path.exists(self.build_path):
            os.makedirs(self.build_path)
        if use_tmpfs == '1':
            tmpfs_size = self.environment.get('TMPFS_SIZE')
            if self.build_path.startswith('/'):
                _build_path = self.build_path
            else:
                _build_path = os.path.join(os.getcwd(), self.build_path)
            if (not misc.path_is_mounted(_build_path)) and tmpfs_size:
                command = 'mount -t tmpfs -o size=%s,mode=0755 tmpfs %s' % (tmpfs_size, self.build_path)
                sys.stdout.write('I: mount tmpfs to build path: %s \n' % self.build_path)
                execute(command)
        execute('/usr/sbin/debootstrap ' + debootstrap_options, env=self.env)
        os.makedirs('.build', exist_ok=True)
        open('.build/bootstrap', 'w').close()

    def setup_chroot(self):
        self.create_progress("setup_chroot")
        scripts = [ 'chroot_misc', 'chroot_apt', 'chroot_dpkg-divert', 'chroot_miscfs' ]
        for script in scripts:
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute("%s install" % os.path.join(self.path, 'scripts/build', script), env=self.env)

    def install_packages(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'chroot_local-packages-list')):
            self.create_progress("chroot_local-package-list")
            execute("%s install" % os.path.join(self.path, 'scripts/build', 'chroot_local-packages-list'), env=self.env)

        scripts = [ 'chroot_packages-list', 'chroot_linux-image', 'chroot_install-packages' ]
        for script in scripts:
            self.create_progress(script)
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute("%s" % os.path.join(self.path, 'scripts/build', script), env=self.env)

        if os.path.exists(os.path.join(self.path, 'scripts/build', 'chroot_local-packages-list')):
            execute("%s remove" % os.path.join(self.path, 'scripts/build', 'chroot_local-packages-list'), env=self.env)

    def run_chroot_hooks(self):
        self.create_progress("chroot_hooks")
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'chroot_hooks')):
            execute(os.path.join(self.path, 'scripts/build', 'chroot_hooks'), env=self.env)

    def run_includes_chroot(self):
        self.create_progress("chroot_includes")
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'chroot_includes')):
            execute(os.path.join(self.path, 'scripts/build', 'chroot_includes'), env=self.env)

    def build_binary_repo(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'binary_packages-list')):
            execute(os.path.join(self.path, 'scripts/build', 'binary_packages-list'), env=self.env)

    def generate_rootfs(self):
        scripts = ['binary_includes', 'binary_linux-image', 'binary_chroot', 'binary_manifest', 'binary_archives', 'binary_rootfs', 'binary_syslinux', 'binary_grub', 'binary_efi_grub2', 'binary_disk', 'binary_iso' ]
        for script in scripts:
            self.create_progress(script)
            if script == "binary_efi_grub2" and self.env.get("LB_SECURITY_BOOT") == '0':
                script = "binary_efi"

            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute(os.path.join(self.path, 'scripts/build', script), env=self.env)

    def demote_chroot(self):
        scripts = [ 'chroot_misc', 'chroot_apt', 'chroot_dpkg-divert', 'chroot_miscfs' ]
        for script in scripts:
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute("%s remove" % os.path.join(self.path, 'scripts/build', script), check=False, env=self.env)

    def run(self):
        sys.stdout.write("Chroot config...\n") 
        self.chroot_config()
        sys.stdout.write("Debootstraping ...\n") 
        self.debootstrap()
        sys.stdout.write("Setup chroot ...\n") 
        self.setup_chroot()
        sys.stdout.write("Installing packages ...\n") 
        self.install_packages()
        sys.stdout.write("Including chroot ...\n")
        self.run_includes_chroot()
        self.run_chroot_hooks()
        self.build_binary_repo()
        self.demote_chroot()
        self.generate_rootfs()
    
if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-p", "--work-path", dest="workpath", help="Set work directory")
    parser.add_option("-a", "--arch", dest="arch", help="Set arch to build")
    parser.add_option("-n", "--name", dest="name", help="Set iso name prefix")
    (options, args) = parser.parse_args()

    if options.workpath is None:
        workpath = os.getcwd()
    else:
        workpath = options.workpath

    support_actions = ['build', 'clean']

    action = None
    for arg in args:
        if arg in support_actions:
            action = arg
            break
    if action is None:
        sys.stderr.write("None of action speicied\n")
        sys.stderr.write("  actions: [%s]\n" % " ".join(support_actions))
        sys.exit(2)
    if action == "clean":
        dirs = ['binary', 'chroot', '.build']
        sys.stdout.write("Clean build workpath...\n")
        for dir in dirs:
            if dir == "chroot" and misc.path_is_mounted(os.path.join(workpath, dir)):
                sys.stdout.write("umount -l %s\n" % os.path.join(workpath, dir))
                execute("umount -l %s" % os.path.join(workpath, dir))
            else:
                sys.stdout.write("rm -rf %s\n" % os.path.join(workpath, dir))
                execute("rm -rf %s" % os.path.join(workpath, dir))

        if os.path.exists(os.path.join(workpath, 'overlay')):
            if misc.path_is_mounted(os.path.join(workpath, 'overlay')):
                execute("umount -l %s" % os.path.join(workpath, 'overlay'))

            execute("rm -rf %s" % os.path.join(workpath, 'overlay'))
        sys.exit(0)

    if not os.path.exists(os.path.join(workpath,'config')):
        sys.stderr.write("WorkPath: %s is not exists.\n" % os.path.join(workpath, 'config'))
        sys.exit(2)

    db = DebianBuilder(architecture=options.arch, name=options.name)
    _current_path = os.getcwd()
    _build_successful = False
    try:
        os.chdir(workpath)
        db.run()
        _build_successful = True
    except Exception as e:
        sys.stderr.write(str(e)+'\n')
        db.demote_chroot()
    finally:
        if os.path.exists(os.path.join(workpath, '.lock')):
            os.system('rm -f %s' % os.path.join(workpath, '.lock'))
        os.chdir(_current_path)

    if _build_successful is True:
        sys.stdout.write("Build successful.\n")
        sys.exit(0)
    else:
        sys.stderr.write("Build failed.\n")
        sys.stderr.write(" > Please feed with buildlog back.\n")
        sys.exit(1)
