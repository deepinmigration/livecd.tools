#! /usr/bin/env python3
import os
import subprocess
import sys
import configparser

COLORS = dict(
        list(zip([
            'grey',
            'red',
            'green',
            'yellow',
            'blue',
            'magenta',
            'cyan',
            'white',
            ],
            list(range(30, 38))
            ))
        )


def environment(path):
    result = {}
    with open(path) as fp:
        for line in fp.readlines():
            line = line.strip()
            if line is None or line.startswith('#') or line == "":
                pass
            else:
                key, value = line.split('=',1)
                result[key] = value
    return result

def colored(text, color=None):
    """Colorize text.

    Available text colors:
        red, green, yellow, blue, magenta, cyan, white.

    Available text highlights:
        on_red, on_green, on_yellow, on_blue, on_magenta, on_cyan, on_white.

    Available attributes:
        bold, dark, underline, blink, reverse, concealed.

    Example:
        colored('Hello, World!', 'red', 'on_grey', ['blue', 'blink'])
        colored('Hello, World!', 'green')
    """
    fmt_str = '\033[%dm%s'
    if color is not None:
        text = fmt_str % (COLORS[color], text)

    text += '\033[0m'

    return text

class ColoredOutput:
    def __init__(self, org_handle, color):
        self.org_handle = org_handle
        def wrapper_write(x):
            return org_handle.write(colored(x, color=color))
        self.wrapper_write = wrapper_write
    def __getattr__(self, attr):
        return self.wrapper_write if attr == 'write' else getattr(self.org_handle, attr)

class conf(configparser.ConfigParser):
    def __init__(self, defaults=""):
        super().__init__(defaults=defaults)

    def optionxform(self, optionstr):
        return optionstr

def path_is_mounted(path, fstype='tmpfs'):
    with open('/proc/self/mounts', 'r') as mtab:
        for line in mtab.readlines():
            _line = line.split()
            if path == _line[1] and fstype == _line[2]:
                return True
    return False

def umount_path_child(path):
    with open('/proc/self/mounts', 'r') as mtab:
        mounted_path=[]
        for line in mtab.readlines():
            _line = line.split()
            if _line[1].startswith(path) and _line[1] != path:
                mounted_path.append(_line[1])
        mounted_path.sort(reverse=True)
    for p in mounted_path:
        execute("umount -f %s" % p)

def execute(command, env=None, check=True):
    if env is not None:
        proc = subprocess.Popen(command, env=env, shell=True, stdout=sys.stdout, stderr=sys.stderr)
    else:
        proc = subprocess.Popen(command, shell=True, stdout=sys.stdout, stderr=sys.stderr)

    proc.communicate()
    if check is True and proc.returncode != 0:
        raise ExecutionException(command, proc.returncode)
    else:
        pass

def round_off(path=None):
    if path is None:
        path=os.getcwd()
    try:
        umount_path_child(path)
        for folder in ['binary', 'chroot', '.build']:
            execute("rm -rf %s" % folder)
    except ExecutionException as e:
        print(e)
        pass

class InitException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return("E: Init exception - %s" % self.info)

class DebootstrapException(Exception):
    def __init__(self, info):
        self.info = info

    def __str__(self):
        return("E: Debootstrap exception - %s" % self.info)

class ExecutionException(Exception):
    def __init__(self, command, status, stderr=None):
        self.stderr = stderr
        self.status = status
        self.command = command

    def __str__(self):
        if self.stderr is not None:
            return ("'%s' returned exit status %s: %s" % (self.command, self.status, self.stderr))
        else:
            return ("'%s' returned exit status %s" % (self.command, self.status))
